import asyncio
import http
import signal
import sys
import time
import ws
import websockets



async def health_check(path, request_headers):
    if path == "/healthz":
        return http.HTTPStatus.OK, [], b"OK\n"
    if path == "/inemuri":
        loop = asyncio.get_running_loop()
        loop.call_later(1, time.sleep, 10)
        return http.HTTPStatus.OK, [], b"Sleeping for 10s\n"
    if path == "/seppuku":
        loop = asyncio.get_running_loop()
        loop.call_later(1, sys.exit, 69)
        return http.HTTPStatus.OK, [], b"Terminating\n"

# async def shutdown(signal, loop):
#     print(f"Received exit signal {signal.name}. Closing connections...")

#     # Gracefully close WebSocket connections
#     for con in ws.connected.copy():
#         await con.close()

#     # Stop the WebSocket server
#     ws.server.close()
#     await ws.server.wait_closed()

#     # Stop the event loop
#     loop.stop()
    

async def main():
    # Set the stop condition when receiving SIGTERM.
    loop = asyncio.get_running_loop()
    stop = loop.create_future()
    loop.add_signal_handler(signal.SIGTERM, stop.set_result, None)

    # Add the shutdown handler for SIGINT
    #loop.add_signal_handler(signal.SIGINT, lambda: asyncio.create_task(shutdown(signal.SIGINT, loop, ws.server)).result())
    # loop.add_signal_handler(signal.SIGINT, lambda: asyncio.create_task(shutdown(signal.SIGINT, loop)))
    
    async with websockets.serve(
        ws.server,
        host="",
        port=80,
        process_request=health_check,
    ):
        await stop


if __name__ == "__main__":
    asyncio.run(main())
