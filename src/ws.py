import asyncio
import websockets
import json
import events
import game
import constants as c
from json_checker import Checker, Or
import traceback

expected_schema = {'event-id': str, 'event-name': str, 'event-data': Or(str, dict, None)}
checker = Checker(expected_schema)

connected = set()
games = {}

def remove_extra_closing_braces(msg):
    open_braces = msg.count('{')
    close_braces = msg.count('}')
    if close_braces > open_braces:
        msg = msg[:open_braces - close_braces]
    return msg

def extract_valid_json(json_str):
    valid_json = ''
    start_index = 0
    try:
        while True:
            valid_part = json.loads(json_str[start_index:])
            valid_json += json.dumps(valid_part)
            break
    except json.JSONDecodeError as e:
        if e.msg == "Extra data":
            valid_json += json_str[start_index:e.pos]
            start_index += e.pos
        else:
            return valid_json
    return valid_json

async def handle_message(websocket, message):
    try:
        message = extract_valid_json(message)
        # message = remove_extra_closing_braces(message.strip())

        message = checker.validate(json.loads(message))
        
        if message[c.EVENT_NAME] == c.DISPATCH_EVENT:
            message[c.EVENT_NAME] = message[c.EVENT_DATA]
            for con in connected:
                if con != websocket:
                    await con.send(json.dumps(message))
        else:
            name = message[c.EVENT_NAME]
            if message[c.EVENT_NAME] in [c.INIT_HOST, c.INIT_JOIN]:
                await events.events[name](websocket, message )   
            else: 
                result = events.try_handle_event(message, websocket)

                if result is not None:
                    print(f"Result: {str(result)}")
                    await websocket.send(json.dumps(result))
                else:
                    await websocket.send({})
    except Exception as e:
        print(f"Error handling message: {e}")
        # Optionally, log the traceback 
        traceback.print_exc()


async def server(websocket, path):
    if websocket not in connected:
        connected.add(websocket)
    try:
        async for message in websocket:
            print(f"Received on server: {str(message)}")
            await handle_message(websocket, message)

    except websockets.exceptions.ConnectionClosed as e:
        print(f"Connection closed. {e}, Reason: {e.reason}")
    finally:
        connected.remove(websocket)
