FROM python:3.12-slim

# Set the working directory to /code
WORKDIR /code

# # Update and upgrade system packages
# RUN apt-get update && apt-get upgrade -y

# Copy requirements.txt to the working directory
COPY ./requirements.txt .
# Install any needed packages specified in requirements.txt

RUN pip install --no-cache-dir -r requirements.txt

# Copy src to the src directory inside the container
COPY ./src ./src


CMD ["python3", "src/main.py", "--host", "0.0.0.0", "--port", "80", "--reload"]

